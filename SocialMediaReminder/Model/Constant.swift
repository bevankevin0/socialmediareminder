//
//  Constant.swift
//  SocialMediaReminder
//
//  Created by bevan christian on 28/08/21.
//

import Foundation
import SwiftUI

enum errorCoreData: Error {
    case contentEmpty
}

enum socialMedia: String, CaseIterable{
    case instagram = "Instagram"
    case tiktok = "Tik-Tok"
    case twitter = "Twitter"
    case faceBook = "FaceBook"
    
}

enum errorThumbnail: Error {
    case videoError
}


enum formatPredicate: String {
    case late = "%K < %@"
    case notLate = "%K > %@"
    case today = "%K >= %@"
}


struct theme {
    var primaryColor = Color("PrimaryColor")
    var navbarLight = Color("navbarLight")
    var backgroundColorThemes = Color("backgroundAziz")
    var tabBarSelected = Color("tabBarSelected")
    var tabBarUnselected = Color("tabBarUnselected")
    var tabBar = Color("tabBar")
    var navigationColor = Color("NavigationColor")
}



extension Color {
    static let themes = theme()
}

func writeToLocal(imageData: Data,title: String) -> URL {
    let fileManager = FileManager.default
    let tmpSubFolderName = ProcessInfo.processInfo.globallyUniqueString
    let tmpSubFolderURL = URL(fileURLWithPath: NSTemporaryDirectory()).appendingPathComponent(tmpSubFolderName, isDirectory: true)
    do {
        let image = UIImage(data: imageData) ?? UIImage()
        try fileManager.createDirectory(at: tmpSubFolderURL, withIntermediateDirectories: true, attributes: nil)
        let imageFileIdentifier = title+".png"
        let fileURL = tmpSubFolderURL.appendingPathComponent(imageFileIdentifier)
        if let imageData = image.pngData() {
            try imageData.write(to: fileURL)
            return fileURL
        }
    }  catch {
        print("error " + error.localizedDescription)
    }
    return URL(string: "")!
}
