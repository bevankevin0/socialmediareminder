//
//  OnboardingModel.swift
//  SocialMediaReminder
//
//  Created by bevan christian on 13/09/21.
//

import Foundation


struct OnboardingModel: Identifiable {
    var id = UUID()
    var imageName: String
    var title: String
    var description: String
}
