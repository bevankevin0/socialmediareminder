//
//  CoreDataProtocol.swift
//  SocialMediaReminder
//
//  Created by bevan christian on 07/09/21.
//

import Foundation
import Combine


protocol CoreDataProtocol {
    func getAllContentBySocialMedia(socialMedia:String) -> Future<[Konten],errorCoreData>
    func getFilterContentByLateness(isShared:Bool,isLate:formatPredicate) -> Future<[Konten],errorCoreData>
    func getAllContentToday() -> Future<[Konten],errorCoreData>
    func insertContent(newContent: TemporaryContent)
    func updateContent(_ konten: TemporaryContent)
    func updateShared(_ konten: TemporaryContent) 
    func deleteContent(_ name:String) -> Future<Bool,errorCoreData>
}
