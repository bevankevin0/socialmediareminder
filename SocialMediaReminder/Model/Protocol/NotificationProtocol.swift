//
//  NotificationProtocol.swift
//  SocialMediaReminder
//
//  Created by bevan christian on 16/09/21.
//

import Foundation


protocol NotificationProtocol {
    func requestAuthorization()
    func createNotification(title:String, date:Date, caption:String, socialType:String,url:String,counter: Double, completion:@escaping(String)->Void)
    func cancelOldNotification(uid:String)
}
