//
//  SocialMediaListSelectionModel.swift
//  SocialMediaReminder
//
//  Created by bevan christian on 03/04/22.
//

import Foundation

struct SocialMediaSelectionModel: Identifiable {
    var id = UUID()
    var name: String
    var number: Int
    var selected: Bool
}
