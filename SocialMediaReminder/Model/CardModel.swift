//
//  CardModel.swift
//  SocialMediaReminder
//
//  Created by bevan christian on 31/12/21.
//

import Foundation
import UIKit

struct CardModel: Identifiable {
    let id = UUID()
    var socialMediaName: String
    var caption: String = "All Content Is On Schedule"
    var thumbnailImage: [UIImage] = [UIImage]() {
        didSet {
            if thumbnailImage.count > 0 {
                self.caption = "You Have \(thumbnailImage.count) Content Late"
            }
        }
    }
}
