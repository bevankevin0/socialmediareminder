//
//  TemporaryContent.swift
//  SocialMediaReminder
//
//  Created by bevan christian on 29/08/21.
//

import Foundation
import SwiftUI

struct TemporaryContent: Identifiable {
    var id:String
    var name:String?
    var caption:String?
    var isShared:Bool?
    var scheduledPost:Date?
    var socialMediaType:String?
    var imageData:Data?
    var videoUrl:String?
    var videoUrlNotif:String?
    var thumbnail:UIImage?
    var uploadDate:Date?
    var notifUid:String?
    var hastag: String = ""
}
