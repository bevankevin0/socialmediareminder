//
//  PhotoPickerModel.swift
//  SocialMediaReminder
//
//  Created by bevan christian on 01/09/21.
//

import Foundation
import PhotosUI

struct PhotoPickerModel {
    enum MediaType {
        case photo, video
    }
    
    var id: String
    var photo: UIImage?
    var url: URL?
    var notifUrl: URL?
    var mediaType: MediaType = .photo
    
    init(with photo: UIImage) {
        id = UUID().uuidString
        self.photo = photo
        mediaType = .photo
    }
    
    init(with videoURL: URL,notifUrl: URL) {
        id = UUID().uuidString
        url = videoURL
        self.notifUrl = notifUrl
        mediaType = .video
    }
    
    
    mutating func delete() {
        switch mediaType {
        case .photo: photo = nil
        case .video:
            guard let url = url else { return }
            try? FileManager.default.removeItem(at: url)
            self.url = nil
        }
    }
}
