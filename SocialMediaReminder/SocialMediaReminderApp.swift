//
//  SocialMediaReminderApp.swift
//  SocialMediaReminder
//
//  Created by bevan christian on 27/08/21.
//
//TESTING 12234
import SwiftUI

@main
struct SocialMediaReminderApp: App {
    @State var isOldUser = UserDefaults.standard.bool(forKey: "oldUser")
    init() {
        UITextView.appearance().backgroundColor = .clear
        UITabBar.appearance().backgroundColor = UIColor(Color.themes.tabBar)
        UITabBar.appearance().unselectedItemTintColor = UIColor(Color.themes.tabBarUnselected)
        UINavigationBar.appearance().backgroundColor = UIColor( Color.themes.backgroundColorThemes)
    }
    var body: some Scene {
        WindowGroup {
            if isOldUser == false {
                OnboardingTabView(isOldUser: $isOldUser)
            } else {
                TabView {
                    HomepageView()
                        .onAppear(perform: UIApplication.shared.addTapGestureRecognizer)
                        .tabItem {
                            VStack {
                                Image(systemName: "house.fill")
                                Text("Home")
                            }
                        }
                    ScheduledView()
                        .onAppear(perform: UIApplication.shared.addTapGestureRecognizer)
                        .tabItem {
                            VStack {
                                Image(systemName: "bell.fill")
                                Text("Scheduled")
                            }
                        }
                }
                .accentColor(Color.themes.tabBarSelected)
            }
        }
        
    }
}
extension UIApplication {
    func addTapGestureRecognizer() {
        guard let window = windows.first else { return }
        let tapGesture = UITapGestureRecognizer(target: window, action: #selector(UIView.endEditing))
        tapGesture.requiresExclusiveTouchType = false
        tapGesture.cancelsTouchesInView = false
        tapGesture.delegate = self
        window.addGestureRecognizer(tapGesture)
    }
}

extension UIApplication: UIGestureRecognizerDelegate {
    public func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldRecognizeSimultaneouslyWith otherGestureRecognizer: UIGestureRecognizer) -> Bool {
        return true // set to `false` if you don't want to detect tap during other gestures
    }
}
