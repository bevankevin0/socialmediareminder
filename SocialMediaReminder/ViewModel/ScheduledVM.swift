//
//  ScheduledVM.swift
//  SocialMediaReminder
//
//  Created by bevan christian on 30/08/21.
//

import Foundation
import SwiftUI
import Combine

class ScheduledVM: ObservableObject {
    @Published var search = ""
    @Published var kontenData = [Konten]()
    @Published var scheduledContent = [TemporaryContent]()
    @Published var searchScheduledContent = [TemporaryContent]()
    @Published var contentSceduledSelection = 1
    @Published var onSearch = false
    let thumbNailService = ThumbnailService()
    var coreDataService: CoreDataProtocol
    var subscription = Set<AnyCancellable>()
    
    
    init(coreDataService:CoreDataProtocol = CoreDataService.shared) {
        self.coreDataService = coreDataService
        subscriber()
        monitorTagSelected()
        monitorSearch()
    }
    
    
    func monitorSearch() {
        $search
            .debounce(for: 0.8, scheduler: DispatchQueue.main)
            .sink { [weak self] searchKeyword in
                self?.searchScheduledContent.removeAll()
                if searchKeyword == "" {
                    self?.onSearch = false
                } else {
                    self?.onSearch = true
                }
                self?.searchScheduledContent = self?.scheduledContent.filter { konten in
                    konten.name?.lowercased().contains(searchKeyword.lowercased()) ?? false
                } ?? [TemporaryContent]()
        }.store(in: &subscription)
    }
    
    func subscriber() {
        thumbNailService.dataFinalThumbnail
            .sink { completion in
                switch completion {
                case .finished:
                    print("finished")
                case .failure(let error):
                    print(error.localizedDescription)
                }
            } receiveValue: { [weak self] hasilThumbnail in
                self?.scheduledContent = hasilThumbnail
            }.store(in: &subscription)
    }
    
    func monitorTagSelected() {
        $contentSceduledSelection.sink { [weak self] tag in
            print(tag)
            if tag == 0 {
                // dapetin yang hari ini
                self?.getTodayContent()
            } else if tag == 1 {
                // on schedule jadwal masih didepan dan belum di shared
                self?.getAllScheduledContentAlreadyPosting()
            } else if tag == 2 {
                // late jadi harus not shared dan jadwal yang lama
                self?.getAllScheduledContentNotPosting()
            }
        }.store(in: &subscription)
        
    }
    
    // late
    func getAllScheduledContentNotPosting() {
        coreDataService.getFilterContentByLateness(isShared: false, isLate: .late)
            .sink { completion in
                switch completion {
                case .finished:
                    print("Ok")
                case .failure(let error):
                    print(error.localizedDescription)
                }
            } receiveValue: { [weak self] dataDariCoreData in
                self?.kontenData = dataDariCoreData
                self?.createThumbnail()
            }.store(in: &subscription)
    }
    
    // on scheduled
    func getAllScheduledContentAlreadyPosting() {
        coreDataService.getFilterContentByLateness(isShared: true, isLate: .notLate)
            .sink { completion in
                switch completion {
                case .finished:
                    print("Ok")
                case .failure(let error):
                    print(error.localizedDescription)
                }
            } receiveValue: { [weak self] dataDariCoreData in
                self?.kontenData = dataDariCoreData
                self?.createThumbnail()
            }.store(in: &subscription)
    }
    
    func getTodayContent() {
        coreDataService.getAllContentToday()
            .sink { completion in
                switch completion {
                case .finished:
                    print("Ok")
                case .failure(let error):
                    print(error.localizedDescription)
                }
            } receiveValue: { [weak self] dataDariCoreData in
                self?.kontenData = dataDariCoreData
                self?.createThumbnail()
            }.store(in: &subscription)
    }
    
    
    func createThumbnail() {
        scheduledContent.removeAll()
        thumbNailService.makeThumbNail(kontenData: kontenData)
    }
    
}
