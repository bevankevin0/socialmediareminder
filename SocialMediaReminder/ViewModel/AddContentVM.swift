//
//  AddContentVM.swift
//  SocialMediaReminder
//
//  Created by bevan christian on 28/08/21.
//

import Foundation
import Combine
import SwiftUI
import CoreML
import AVKit

var tomorrow: Date {
    return Calendar.current.date(byAdding: .day, value: 1, to: Date())!
}

class AddContentVM: ObservableObject {
    let model = MobileNetV2()
    @Published var classificationLabel = [String]()
    @Published var postTitle = ""
    @Published var hastag = ""
    @Published var captionContent = ""
    @Published var datePosting = tomorrow
    @Published var isInvalid = true
    @Published var accuracy = "90"
    @Published var socialMediaType = [Int]()
    @Published var itemsContentPicOrVid = PhotoPickerModel(with: UIImage(named: "addImagePlaceholder")!)
    
    private let dates = Date()
    private let notificationService: NotificationProtocol
    private var coreDataService: CoreDataProtocol
    private var subscription = Set<AnyCancellable>()
    
    
    init(notification:NotificationProtocol = NotificationService.shared,coreDataService: CoreDataProtocol = CoreDataService.shared) {
        self.coreDataService = coreDataService
        self.notificationService = notification
        validationForm()
    }
    
    
    func validationForm() {
        $postTitle.combineLatest($captionContent,$itemsContentPicOrVid,$socialMediaType)
            .sink { [weak self] title, caption, gambar, social in
                if title.count > 2 && caption.count > 5 && (gambar.photo != nil || gambar.url != nil) && social.count > 0 {
                    self?.isInvalid = false
                } else {
                    self?.isInvalid = true
                }
            }.store(in: &subscription)
    }
    
    func append(item: PhotoPickerModel) {
        itemsContentPicOrVid = item
        if itemsContentPicOrVid.mediaType == .photo {
            clasifyImage()
        } else {
            clasifyVideo()
        }
        
        
    }
    
    func clasifyImage() {
        guard let image = itemsContentPicOrVid.photo,
              let resizedImage = image.resizeImageTo(size: CGSize(width: 224, height: 224)),
              let buffer = resizedImage.convertToBuffer() else {return}
        let output = try? model.prediction(image: buffer)
        
        creatingHastag(output: output)
        
    }
    
    func clasifyVideo() {
        let videoUrl = itemsContentPicOrVid.url
        guard let videoUrl = videoUrl else {
            print("error video url nil")
            return
        }
        guard let image = getThumbnailFromVideo(url: "\(videoUrl)"),
              let resizedImage = image.resizeImageTo(size: CGSize(width: 224, height: 224)),
              let buffer = resizedImage.convertToBuffer() else {
                  print("error video image gagal")
                  return}
        let output = try? model.prediction(image: buffer)
        
        creatingHastag(output: output)
        
    }
    
    private func creatingHastag(output: MobileNetV2Output?) {
        
        if let output = output {
            // 1
            classificationLabel.removeAll()
            var accuracy = [Double]()
            let results = output.classLabelProbs.sorted { $0.1 > $1.1 }
            
            print(results)
            
            // 2
            let result = results.filter({ (key,value) in
                return value > 0.6
            }).map { (key, value) -> String in
                accuracy.append(value)
                return "#\(key.replacingOccurrences(of: ", ", with: " #"))"
            }
            
            // 3
            for index in 0..<result.count {
                let data = result[index].components(separatedBy: " ")
                let array = data.joined(separator: ", ").replacingOccurrences(of: ", ", with: " #").replacingOccurrences(of: " ##", with: " #")
                classificationLabel += ["\(array) "]
            }
            let sumArray = accuracy.reduce(0, +)

            //TODO: KALAU NAN KASIH NOT FOUND
            if sumArray == 0.0 {
                self.accuracy = "0"
            } else {
                let avgArrayValue = sumArray / Double(accuracy.count)
                self.accuracy = "\(avgArrayValue)"
            }
            hastag = classificationLabel.joined(separator: "")
        }
    }
    
    func getThumbnailFromVideo(url:String) -> UIImage? {
        guard let urlFix = URL(string: url) else { return UIImage()}
        let asset = AVAsset(url: URL(string: url)!)
        let avAssetImageGenerator = AVAssetImageGenerator(asset: asset)
        avAssetImageGenerator.appliesPreferredTrackTransform = true
        let thumnailTime = CMTimeMake(value: 1, timescale: 1)
        do {
            let cgThumbImage = try avAssetImageGenerator.copyCGImage(at: thumnailTime, actualTime: nil)
            let thumbImage = UIImage(cgImage: cgThumbImage)
            return thumbImage
        } catch let error {
            print(error)
        }
        return nil
    }
    
    
    func insertContent(completion: @escaping() -> Void) {
        DispatchQueue.global(qos: .userInitiated).async { [self] in
        if socialMediaType.count == 1 {
            var kontentToUpload = TemporaryContent(id: UUID().uuidString)
            kontentToUpload.name = postTitle
            kontentToUpload.caption = captionContent
            kontentToUpload.isShared = false
            kontentToUpload.scheduledPost = datePosting
            kontentToUpload.hastag = hastag
            kontentToUpload.socialMediaType = socialMedia.allCases[socialMediaType.first ?? 0].rawValue
            if itemsContentPicOrVid.photo != nil {
                kontentToUpload.imageData = itemsContentPicOrVid.photo?.jpegData(compressionQuality: 0.5)
                kontentToUpload.videoUrl = nil
                kontentToUpload.videoUrlNotif = nil
            } else if itemsContentPicOrVid.notifUrl != nil {
                kontentToUpload.videoUrlNotif = itemsContentPicOrVid.notifUrl?.absoluteString
                kontentToUpload.videoUrl = itemsContentPicOrVid.url?.absoluteString
                kontentToUpload.imageData = nil
            }
            let counter: Double = 1
            createNotification(konten: kontentToUpload, counter: counter) { [weak self] uidNotif in
                kontentToUpload.notifUid = uidNotif
                self?.coreDataService.insertContent(newContent: kontentToUpload)
                completion()
            }
        } else if socialMediaType.count > 1 {
            for index in socialMediaType {
                var kontentToUpload = TemporaryContent(id: UUID().uuidString)
                kontentToUpload.name = postTitle
                kontentToUpload.caption = captionContent
                kontentToUpload.isShared = false
                kontentToUpload.scheduledPost = datePosting
                kontentToUpload.hastag = hastag
                kontentToUpload.socialMediaType = socialMedia.allCases[index].rawValue
                if itemsContentPicOrVid.photo != nil {
                    kontentToUpload.imageData = itemsContentPicOrVid.photo?.jpegData(compressionQuality: 0.5)
                    kontentToUpload.videoUrl = nil
                    kontentToUpload.videoUrlNotif = nil
                } else if itemsContentPicOrVid.url != nil {
                    kontentToUpload.videoUrlNotif = itemsContentPicOrVid.notifUrl?.absoluteString
                    kontentToUpload.videoUrl = itemsContentPicOrVid.url?.absoluteString
                    kontentToUpload.imageData = nil
                }
                let counter: Double = Double(index * 20)
                createNotification(konten: kontentToUpload, counter: counter) { [weak self] uidNotif in
                    kontentToUpload.notifUid = uidNotif
                    self?.coreDataService.insertContent(newContent: kontentToUpload)
                }
            }
            completion()
        }
        }
    }
    
    
    
    private func createNotification(konten: TemporaryContent,counter: Double,completion:@escaping (String)-> Void) {
        guard konten.name != nil else {
            return
        }
        
        DispatchQueue.main.async { [weak self] in
            if let url = konten.videoUrlNotif {
                self?.notificationService.createNotification(title: konten.name ?? "" , date: konten.scheduledPost!, caption: konten.caption!, socialType: konten.socialMediaType!, url: url, counter: counter) { uidNotif in
                    completion(uidNotif)
                }
            } else {
                let url = writeToLocal(imageData: konten.imageData ?? Data(), title: konten.name ?? "")
                self?.notificationService.createNotification(title: konten.name! , date: konten.scheduledPost!, caption: konten.caption!, socialType: konten.socialMediaType!, url: "\(url)", counter: counter) { uidNotif in
                    completion(uidNotif)
                }
            }
            
        }
        
    }
}



