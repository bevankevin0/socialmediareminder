//
//  DetailVM.swift
//  SocialMediaReminder
//
//  Created by bevan christian on 31/08/21.
//

import Foundation
import SwiftUI
import Combine


class DetailVM: ObservableObject {
    @Published var showAlert = false
    let notificationService: NotificationProtocol
    var coreDataService: CoreDataProtocol
    var subscription = Set<AnyCancellable>()
    
    init(notification:NotificationProtocol = NotificationService.shared,coreDataService:CoreDataProtocol = CoreDataService.shared) {
        self.coreDataService = coreDataService
        self.notificationService = notification
    }
    
    func updateIsShared(contentData:TemporaryContent) {
        var contentDataShared = contentData
        coreDataService.updateShared(contentDataShared)
    }
        
    func addToClipBoard(caption: String,hastag: String) {
        let pasteBoard = UIPasteboard.general
        
        pasteBoard.string = caption + "\n\(hastag)"
    }
    
    func deleteKonten(_ name: String) {
        coreDataService.deleteContent(name).sink { completion in
            switch completion {
            case .finished:
                print("sukses")
            case .failure(let error):
                print(error.localizedDescription)
            }
        } receiveValue: { [weak self] hasil in
            DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + 0.5) {
                self?.showAlert = hasil
            }
            
        }.store(in: &subscription)
    }
    
    func updateKonten(_ dateChange:Date,uidNotif:String,konten: TemporaryContent) {
        cancelOldNotification(uid: uidNotif)
        DispatchQueue.main.async { [self] in
            if let url = konten.videoUrlNotif {
                notificationService.createNotification(title: konten.name ?? "" , date: konten.scheduledPost!, caption: konten.caption!, socialType: konten.socialMediaType!, url: url, counter: 1) { uidNotif in
                    updateUidInCoreData(konten: konten)
                }
            } else {
                let url = writeToLocal(imageData: konten.imageData ?? Data(), title: konten.name ?? "")
                notificationService.createNotification(title: konten.name! , date: konten.scheduledPost!, caption: konten.caption!, socialType: konten.socialMediaType!, url: "\(url)", counter: 1) { uidNotif in
                    updateUidInCoreData(konten: konten)
                }
            }
            
        }
    }
    
    private func cancelOldNotification(uid:String) {
        notificationService.cancelOldNotification(uid: uid)
    }
    
    private func updateUidInCoreData(konten: TemporaryContent) {
        //self.coreDataService.updateKonten(name, caption: caption, title: title, dateContent: dateChange, uidNotif: uid)
        coreDataService.updateContent(konten)
    }
}
