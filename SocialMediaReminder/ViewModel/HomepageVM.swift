//
//  HomepageVM.swift
//  SocialMediaReminder
//
//  Created by bevan christian on 30/12/21.
//

import Foundation
import CoreData
import UIKit
import Combine

class HomepageVM: ObservableObject {
    @Published var kontens = [Konten]()
    @Published var kontenCard = [CardModel]()
    @Published var EmptykontenCard = [CardModel(socialMediaName: socialMedia.instagram.rawValue),CardModel(socialMediaName: socialMedia.tiktok.rawValue),CardModel(socialMediaName: socialMedia.faceBook.rawValue),CardModel(socialMediaName: socialMedia.twitter.rawValue)]
    @Published var tempkontenData = [TemporaryContent]()
    private let thumbNailService = ThumbnailService()
    private var coreDataService: CoreDataProtocol
    private var subscription = Set<AnyCancellable>()
    
    init(coreService: CoreDataProtocol = CoreDataService.shared) {
        coreDataService = coreService
        subscriber()
    }
    
    func loadData() {
        coreDataService.getFilterContentByLateness(isShared: false, isLate: .late)
            .filter({ data in
                return data.count > 0
            })
        .sink { completion in
            switch completion {
            case .finished:
                print("finished")
            case .failure(let error):
                print(error.localizedDescription)
            }
        } receiveValue: { [weak self] konten in
            self?.kontens = konten
            guard let kontens1 = self?.kontens else { return }
            self?.createThumbnail(dataFromCoreData: kontens1)
        }.store(in: &subscription)
    }
    
    private func createThumbnail(dataFromCoreData: [Konten]) {
        tempkontenData.removeAll()
        thumbNailService.makeThumbNail(kontenData: dataFromCoreData)
    }
    
    // MARK: OBSERVATION
    func subscriber() {
        thumbNailService.dataFinalThumbnail
            .filter({ data in
                return data.count > 0
            })
            .sink { completion in
                switch completion {
                case .finished:
                    print("finished")
                case .failure(let error):
                    print(error.localizedDescription)
                }
            } receiveValue: { [weak self] hasilThumbnail in
                let thumbnailData = hasilThumbnail
                self?.sortingByDate(thumbnailData: thumbnailData)
            }.store(in: &subscription)
    }
    
    private func sortingByDate(thumbnailData: [TemporaryContent]) {
        // variabel
        if thumbnailData.count != 0 {
            let sortedHasil = thumbnailData.sorted(by: { $0.uploadDate! > $1.uploadDate! })
            tempkontenData = sortedHasil
            mappingSocialMedia()
        }
    }
    
    
 
    
    func mappingSocialMedia() {
        var instagram = CardModel(socialMediaName: socialMedia.instagram.rawValue)
        var facebook = CardModel(socialMediaName: socialMedia.faceBook.rawValue)
        var tiktok = CardModel(socialMediaName: socialMedia.tiktok.rawValue)
        var twitter = CardModel(socialMediaName: socialMedia.twitter.rawValue)
        for konten in tempkontenData {
            if konten.socialMediaType == socialMedia.instagram.rawValue {
                instagram.thumbnailImage.append(konten.thumbnail ?? UIImage())
            } else if konten.socialMediaType == socialMedia.faceBook.rawValue {
                facebook.thumbnailImage.append(konten.thumbnail ?? UIImage())
            } else if konten.socialMediaType == socialMedia.tiktok.rawValue {
                tiktok.thumbnailImage.append(konten.thumbnail ?? UIImage())
            } else if konten.socialMediaType == socialMedia.twitter.rawValue {
                twitter.thumbnailImage.append(konten.thumbnail ?? UIImage())
            }
        }
        kontenCard = [instagram,facebook,tiktok,twitter]
    }
    
}
