//
//  HomepageViewModel.swift
//  SocialMediaReminder
//
//  Created by bevan christian on 29/08/21.
//

import Foundation
import Combine
import SwiftUI
import CoreData



class ContentVM: ObservableObject {
    @Published var tempkontenData = [TemporaryContent]() // ini ke view
    private let thumbNailService = ThumbnailService()
    private let coreDataService: CoreDataProtocol
    private var subscription = Set<AnyCancellable>()

    init(coreDataService: CoreDataProtocol = CoreDataService.shared) {
        self.coreDataService = coreDataService
        self.subscriber()
    }
    
    func subscriber() {
        thumbNailService.dataFinalThumbnail
            .sink { completion in
                switch completion {
                case .finished:
                    print("finished")
                case .failure(let error):
                    print(error.localizedDescription)
                }
            } receiveValue: { [weak self] hasilThumbnail in
                let thumbnailData = hasilThumbnail
                self?.sortingByDate(thumbnailData: thumbnailData)
            }.store(in: &subscription)
    }
    
    private func sortingByDate(thumbnailData: [TemporaryContent]) {
        // variabel
        if thumbnailData.count != 0 {
            let sortedHasil = thumbnailData.sorted(by: { $0.uploadDate! > $1.uploadDate! })
            tempkontenData = sortedHasil
        }
    }

    func getAllContentBySocialMediaType(_ typeOfSocialMedia:String = socialMedia.instagram.rawValue) {
        var dataFromCoreData = [Konten]() // core ke thumbnail
        coreDataService.getAllContentBySocialMedia(socialMedia: typeOfSocialMedia)
            .sink { completion in
                switch completion {
                case .finished:
                    print("Ok")
                case .failure(let error):
                    print(error.localizedDescription)
                }
            } receiveValue: { [weak self] dataDariCoreData in
                dataFromCoreData = dataDariCoreData
                // skip ini
                self?.createThumbnail(dataFromCoreData: dataFromCoreData)
            }.store(in: &subscription)
    }
    
    private func createThumbnail(dataFromCoreData: [Konten]) {
        tempkontenData.removeAll()
        thumbNailService.makeThumbNail(kontenData: dataFromCoreData)
        
    }

    
}

