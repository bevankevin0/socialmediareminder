//
//  ScheduledView.swift
//  SocialMediaReminder
//
//  Created by bevan christian on 27/08/21.
//

import SwiftUI

struct PickerModel: Identifiable {
    var id = UUID()
    var name : String
    var selected : Bool = false
}

struct ScheduledView: View {
    @Environment(\.colorScheme) var colorScheme
    @StateObject var scheduledVm = ScheduledVM()
    @State var detailPageShow = false
    @State var addPageShow = false
    @State var index = 0
    @State var isLoading = false
    @State var picker = [PickerModel(name: "Today",selected: true),PickerModel(name: "On Scheduled"),PickerModel(name: "Late")]
    var body: some View {
        NavigationView {
            GeometryReader { geo in
                ZStack {
                    Color.themes.backgroundColorThemes
                    VStack {
                        VStack {
                            SearchBar(text: $scheduledVm.search)
                                .padding(.horizontal,5)
                            HStack {
                                ForEach(0..<picker.count) { index in
                                    Button {
                                        removeAllSelected()
                                        picker[index].selected = true
                                        self.index = index
                                        scheduledVm.contentSceduledSelection = index
                                    } label: {
                                        Text("\(picker[index].name)")
                                            .font(Font.custom("AvenirNextLTPro-Bold", size: 13))
                                            .frame(width: geo.size.width * 0.3, height: 40)
                                            .background(picker[index].selected == true ? Color.black : Color.white)
                                            .foregroundColor(picker[index].selected == true ? Color.white : Color.black)
                                            .cornerRadius(10)
                                    }
                                    
                                }
                            }
                        }
                        .padding(.top,50)
                        ScrollView(showsIndicators: false) {
                            PullToRefresh(coordinateSpaceName: "pullToRefresh") {
                                print("indeks \(index)")
                                if isLoading == false {
                                    print("indeks \(index)")
                                    scheduledVm.contentSceduledSelection = index
                                    isLoading = true
                                    DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + 0.2) {
                                        isLoading = false
                                    }
                                }
                            }
                            if scheduledVm.scheduledContent.count == 0 {
                                VStack {
                                    Spacer()
                                    LottieView(name: "emptyAnimation",loopMode: .loop)
                                        .frame(width: 250, height: 200)
                                    Text("No Content Today")
                                        .font(Font.custom("AvenirNextLTPro-regular", size: 13))
                                        .offset(y: -50)
                                    Spacer()
                                }
                            } else {
                                if scheduledVm.onSearch {
                                    ForEach(scheduledVm.searchScheduledContent,id:\.id, content: { row in
                                        NavigationLink(destination: DetailPageView(kontenData: row)) {
                                            ScheduledCardView(datePassing: row.scheduledPost ?? Date(), title: row.name ?? "", image: row.thumbnail ?? UIImage(named: "Nature")!, socialMediaType: row.socialMediaType ?? socialMedia.instagram.rawValue)
                                                .background(Color.white)
                                                .cornerRadius(10)
                                                .padding(.horizontal,10)
                                                .padding(.vertical,4)
                                        }
                                    }
                                    )
                                } else {
                                    ForEach(scheduledVm.scheduledContent,id:\.id, content: { row in
                                        NavigationLink(destination: DetailPageView(kontenData: row)) {
                                            ScheduledCardView(datePassing: row.scheduledPost ?? Date(), title: row.name ?? "", image: row.thumbnail ?? UIImage(named: "Nature")!, socialMediaType: row.socialMediaType ?? socialMedia.instagram.rawValue)
                                                .background(Color.white)
                                                .cornerRadius(10)
                                                .padding(.horizontal,10)
                                                .padding(.vertical,4)
                                        }
                                    }
                                    )
                                }
                                
                            }
                            Spacer()
                                .navigationTitle("Scheduled")
                        }
                        .coordinateSpace(name: "pullToRefresh")
                        .frame(width: geo.size.width * 1, height: geo.size.height * 0.75)
                    }
                    .ignoresSafeArea(.keyboard, edges: .bottom)
                    .frame(width: geo.size.width * 1, height: geo.size.height * 1)
                    .navigationBarItems(
                        trailing:
                            Button(action: {
                                addPageShow.toggle()
                            }, label: {
                                Text("Add")
                            }
                                  )
                    )
                    
                }
                .ignoresSafeArea()
            }
            .ignoresSafeArea(.keyboard)
            
        }
        .accentColor(Color.themes.navigationColor)
        .onAppear {
            scheduledVm.getTodayContent()
        }.sheet(isPresented: $addPageShow, onDismiss: {
            scheduledVm.getTodayContent()
        }, content: {
            AddContentView()
        })
    }
    func removeAllSelected() {
        for index in 0..<picker.count {
            picker[index].selected = false
        }
    }
    
}

struct ScheduledView_Previews: PreviewProvider {
    static var previews: some View {
        ScheduledView()
    }
}



extension Date {
    
    func toNiceDate() -> String {
        let formatter = DateFormatter()
        formatter.dateFormat = "EEEE, MMM d, yyyy"
        return formatter.string(from: self)
    }
    
}

