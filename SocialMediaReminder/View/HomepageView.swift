//
//  HomepageView.swift
//  SocialMediaReminder
//
//  Created by bevan christian on 28/08/21.
//

import SwiftUI

struct HomepageView: View {
    @Environment(\.colorScheme) var colorScheme
    @StateObject var homeVm = HomepageVM()
    @State var selection = ""
    @State var themeColor = Color.themes.navbarLight
    var body: some View {
        NavigationView {
            GeometryReader { geo in
                ZStack {
                    Color.themes.backgroundColorThemes
                    VStack(spacing:10) {
                        Spacer()
                        ScrollView(showsIndicators: false) {
                            if homeVm.kontenCard.isEmpty {
                                ForEach(homeVm.EmptykontenCard) { konten in
                                    NavigationLink {
                                        ContentView(socialMediaPicked: konten.socialMediaName)
                                    } label: {
                                        CardView(imageName: konten.socialMediaName, socialMediaName:  konten.socialMediaName, announcment: konten.caption,listThumbnail: konten.thumbnailImage)
                                            .frame(width: geo.size.width * 0.9)
                                            .padding(.top,8)
                                    }
                                    .buttonStyle(PlainButtonStyle())
                                }
                            } else {
                                ForEach(homeVm.kontenCard) { konten in
                                    NavigationLink {
                                        ContentView(socialMediaPicked: konten.socialMediaName)
                                    } label: {
                                        CardView(imageName: konten.socialMediaName, socialMediaName:  konten.socialMediaName, announcment: konten.caption,listThumbnail: konten.thumbnailImage)
                                            .frame(width: geo.size.width * 0.9)
                                            .padding(.top,8)
                                           
                                    }
                                    .buttonStyle(PlainButtonStyle())
                                }
                            }
                            
                            NavigationLink {
                                EmptyView()
                            } label: {
                                EmptyView()
                            }
                            
                            NavigationLink {
                                EmptyView()
                            } label: {
                                EmptyView()
                            }

                      
                        }
                        .padding(.top,30)
                        .foregroundColor(colorScheme == .dark ? Color.black : Color.black)
                        .frame(width: geo.size.width * 1, height: geo.size.height * 0.95)
                        
                    }
                    .frame(width: geo.size.width * 1, height: geo.size.height * 1)
                    .navigationTitle("Home")
                    
                }
                .ignoresSafeArea()
            }
            .onAppear {
                homeVm.loadData()
            }
        }
        .accentColor(Color.themes.navigationColor)
    }
}

struct HomepageView_Previews: PreviewProvider {
    static var previews: some View {
        HomepageView()
            .preferredColorScheme(.dark)
        
        HomepageView()
            .preferredColorScheme(.light)
    }
}

