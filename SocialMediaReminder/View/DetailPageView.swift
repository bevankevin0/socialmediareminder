//
//  DetailPageView.swift
//  SocialMediaReminder
//
//  Created by bevan christian on 28/08/21.
//

import SwiftUI
import Introspect
import AVKit
import Combine


struct DetailPageView: View {
    @Environment(\.presentationMode) var presentationMode: Binding<PresentationMode>
    @Environment(\.colorScheme) var colorScheme
    @State var postTitle = ""
    @State var captionContent = "Please fill the caption"
    @State var datePosting = Date()
    @State var uiTabarController: UITabBarController?
    var kontenData:TemporaryContent
    @State var date = ""
    @State var wordContent = ""
    @State var shareSheet = false
    @State var deleteSucses = false
    @State var isEdit = false
    @State var hastag = ""
    @State var showConfirmation = false
    var subscription = Set<AnyCancellable>()
    @StateObject var detailVm = DetailVM()
    var body: some View {
        GeometryReader { geo in
            ZStack {
                if colorScheme == .light {
                    Color.themes.backgroundColorThemes
                } else {
                    Color.themes.backgroundColorThemes
                }
                ScrollView {
                    VStack {
                        if kontenData.thumbnail != nil && kontenData.videoUrl == nil {
                            Image(uiImage: (kontenData.thumbnail ?? UIImage(named: "Nature"))!)
                                .resizable()
                                .scaledToFit()
                                .clipped()
                                .frame(width: geo.size.width, height: geo.size.height * 0.40)
                                .padding(.top,90)
                        } else if kontenData.videoUrl != nil {
                            if let url = kontenData.videoUrl {
                                
                                VideoPlayer(player: AVPlayer(url: URL(string:url)!))
                                    .frame(width: geo.size.width, height: geo.size.height * 0.3)
                                    .ignoresSafeArea(.all)
                                    .padding(.top,120)
                                
                            } else { EmptyView() }
                        }
                        TextFieldCustom(value: $postTitle, title: "Content Title", prompt: "\(kontenData.name)", footer: "")
                            .disabled(true)
                        TextEditorCustom(value: $wordContent, title: "Caption", prompt: wordContent, footer: "")
                            .disabled(true)
                        TextEditorCustom(value: $hastag, title: "Hastag", prompt: hastag, footer: "")
                            .disabled(true)
                        TextFieldCustom(value: $date, title: "Posting Time", prompt: "\(date)", footer: "")
                            .disabled(true)
                        Button(action: {
                            detailVm.addToClipBoard(caption: kontenData.caption ?? "",hastag: kontenData.hastag ?? "")
                            detailVm.updateIsShared(contentData: kontenData)
                            shareSheet = true
                        }, label: {
                            Text("Share")
                                .frame(width: geo.size.width * 0.8 , height: 40, alignment: .center)
                                .background(colorScheme == .light ? Color.black : Color.blue)
                                .foregroundColor(colorScheme == .light ? Color.white : Color.white)
                                .cornerRadius(10)
                        }).padding()
                    }
                }
            }.ignoresSafeArea(.all)
            
            
        }
        .onDisappear(perform: {
            uiTabarController?.tabBar.isHidden = false
        })
        .onAppear(perform: {
            wordContent = kontenData.caption ?? ""
            postTitle = kontenData.name ?? ""
            hastag = kontenData.hastag
            date = formatingDate(date: kontenData.scheduledPost!)
        })
        .navigationBarItems(
            trailing:
                HStack {
                    Button(action: {
                        showConfirmation.toggle()
                        detailVm.showAlert = true
                    }, label: {
                        Image(systemName:"trash.fill")
                    }
                    ).padding(.trailing,10)
                    Spacer()
                    Button(action: {
                        isEdit = true
                    }, label: {
                        Image(systemName:"pencil.tip.crop.circle")
                    }
                    ).sheet(isPresented: $isEdit) {
                        EditContentView(title: postTitle, caption: wordContent, hastag: hastag, dateSelection: kontenData.scheduledPost!, uidNotif: kontenData.notifUid ?? "0", socialMediaType: kontenData.socialMediaType ?? "Instagram", kontent: kontenData, detailVm: detailVm)
                    }
                }
               
        )
        .navigationTitle("Detail Page")
        .navigationBarTitleDisplayMode(.inline)
        .introspectTabBarController { (UITabBarController) in
            UITabBarController.tabBar.isHidden = true
            uiTabarController = UITabBarController
        }.sheet(isPresented: $shareSheet) {
            if kontenData.videoUrl == nil {
                ShareSheetView(thumbnail: kontenData.thumbnail, contentName: kontenData.name, activityItems: [kontenData.thumbnail,kontenData.caption,kontenData.hastag])
            } else if kontenData.videoUrl != nil {
                let url = getVideo()
                ShareSheetView(thumbnail: kontenData.thumbnail, contentName: kontenData.name, activityItems: [url])
            }
            
        }.alert(isPresented: $detailVm.showAlert) {
            if showConfirmation {
                return Alert(title: Text("Delete"), message: Text("This Action Cannot be Unndone"), primaryButton: .default(Text("Yes"), action: {
                    detailVm.deleteKonten(kontenData.id ?? "")
                    showConfirmation = false
                }), secondaryButton: .cancel(Text("Cancel"), action: {
                    showConfirmation = false
                }))
            } else {
                return Alert(title: Text("Success"), message: Text("Your content has been deleted successfully"), dismissButton: .default(Text("OK"), action: {
                    presentationMode.wrappedValue.dismiss()
                }))
            }

        }
    }
    
    
    func getVideo() -> URL? {
        return URL(string: kontenData.videoUrl ?? "")
    }
    
    func formatingDate(date:Date) -> String {
        let dateFormatterPrint = DateFormatter()
        dateFormatterPrint.dateFormat = "MMM dd,yyyy, h:mm a"
        return dateFormatterPrint.string(from: date)
    }
}

struct DetailPageView_Previews: PreviewProvider {
    static var previews: some View {
        DetailPageView(kontenData: TemporaryContent(id:"UUID()", name: "hello", caption: "hello", isShared: false, scheduledPost: Date(), socialMediaType: "hello", imageData: Data(), videoUrl: "hello", videoUrlNotif: "", thumbnail: UIImage(), uploadDate: Date(), notifUid: "hello"))
    }
}
