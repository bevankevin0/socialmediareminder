//
//  ContentView.swift
//  SocialMediaReminder
//
//  Created by bevan christian on 27/08/21.
//

import SwiftUI
import CoreData
import Introspect

struct ContentView: View {
    @State var imagePost = "Nature"
    @State var addPageShow = false
    @State var detailPageShow = false
    @State var isAdded = false
    @State var isLoading = false
    @StateObject var homePageVm = ContentVM()
    var socialMediaPicked: String!
    var dataFake = [2]
    
    
    var body: some View {
        GeometryReader { geo in
            ZStack {
                Color.themes.backgroundColorThemes
                VStack {
                    Spacer()
                    ScrollView {
                        if homePageVm.tempkontenData.count == 0 {
                            HStack {
                                Spacer()
                                Text("Add Your Content First")
                                    .foregroundColor(Color.gray)
                                Spacer()
                            }.padding(.top,geo.size.height * 0.5)
                        } else {
                            PullToRefresh(coordinateSpaceName: "pullToRefresh") {
                                if isLoading == false {
                                    print("reload content")
                                    homePageVm.getAllContentBySocialMediaType(socialMediaPicked)
                                    isLoading = true
                                    DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + 0.2) {
                                        isLoading = false
                                    }
                                }
                            }
                            LazyVGrid(columns: [GridItem(.adaptive(minimum: 100, maximum: 300))],
                                      alignment: .center,
                                      spacing: 0,
                                      content: {
                                ForEach(homePageVm.tempkontenData) { row in
                                    NavigationLink(
                                        destination: DetailPageView(kontenData: row),
                                        label: {
                                            VStack {
                                                Image(uiImage: (row.thumbnail ?? UIImage(named: "Nature"))!)
                                                    .resizable()
                                                    .opacity(row.isShared! ? 0.5 : 1)
                                                    .aspectRatio(contentMode: .fill)
                                                    .frame(width: geo.size.width * 0.333, height: geo.size.height * 0.2, alignment: .center)
                                                    .clipped()
                                                    .overlay(ImageOverlay(datePassing: row.scheduledPost!, isShared: row.isShared!), alignment: .bottomTrailing)
                                                    .contextMenu {
                                                        NavigationLink(destination: DetailPageView(kontenData: row), label: {
                                                            Text("Edit visit")
                                                            Image(systemName: "square.and.pencil")
                                                        })
                                                    }
                                                
                                            }
                                        })
                                }
                            })
                            
                                .alert(isPresented: $isAdded) {
                                    Alert(title: Text("Success"), message: Text("Your content has been successfully added, please exit to see it"), dismissButton: .cancel(Text("Ok")))
                                }
                        }
                    }
                    .coordinateSpace(name: "pullToRefresh")
                    .padding(.top,geo.size.height * 0.085)
                }
                .padding(.top)
                
            }
            .ignoresSafeArea()
            .navigationTitle("\(socialMediaPicked)")
            .navigationBarTitleDisplayMode(.inline)
            .navigationBarItems(
                trailing:
                    Button(action: {
                        addPageShow.toggle()
                    }, label: {
                        Text("Add")
                    }
                          )
            )
            .onAppear(perform: {
                homePageVm.getAllContentBySocialMediaType(socialMediaPicked)
            })
            
        }
        .sheet(isPresented: $addPageShow, onDismiss: {
            homePageVm.getAllContentBySocialMediaType(socialMediaPicked)
        }, content: {
            AddContentView(socialMediaPicked: socialMediaPicked)
        })
    }
    
}


struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}

