//
//  PHPickerView.swift
//  SocialMediaReminder
//
//  Created by bevan christian on 01/09/21.
//

import Foundation
import PhotosUI
import SwiftUI



struct PHPickerView: UIViewControllerRepresentable {
    var didFinishPicking: (_ didSelectItems: Bool) -> Void
    typealias UIViewControllerType = PHPickerViewController
    @ObservedObject var mediaItems: AddContentVM
    
    func makeCoordinator() -> Coordinator {
        Coordinator(with: self)
    }
    
    func makeUIViewController(context: Context) -> PHPickerViewController {
        var config = PHPickerConfiguration()
        config.filter = .any(of: [.images, .videos])
        config.selectionLimit = 1
        config.preferredAssetRepresentationMode = .current
        
        let controller = PHPickerViewController(configuration: config)
        controller.delegate = context.coordinator
        return controller
    }
    
    func updateUIViewController(_ uiViewController: PHPickerViewController, context: Context) {
        
    }
    
    class Coordinator: PHPickerViewControllerDelegate {
        var photoPicker: PHPickerView
        init(with photoPicker: PHPickerView) {
            self.photoPicker = photoPicker
        }
        
        func picker(_ picker: PHPickerViewController, didFinishPicking results: [PHPickerResult]) {
            photoPicker.didFinishPicking(!results.isEmpty)
            
            guard !results.isEmpty else {
                return
            }
            
            for result in results {
                let itemProvider = result.itemProvider
                guard let typeIdentfier = itemProvider.registeredTypeIdentifiers.first,
                      let uType = UTType(typeIdentfier)
                else { continue }
                if uType.conforms(to: .image) {
                    self.getPhoto(from: itemProvider, isLivePhoto: false)
                } else if uType.conforms(to: .video) {
                    self.getVideo(from: itemProvider, typeIdentifier: typeIdentfier)
                } else {
                    self.getVideo(from: itemProvider, typeIdentifier: typeIdentfier)
                }
            }
            
            
        }
        
        private func getPhoto(from itemProvider: NSItemProvider, isLivePhoto: Bool) {
            let objectType: NSItemProviderReading.Type = !isLivePhoto ? UIImage.self : PHLivePhoto.self
            
            if itemProvider.canLoadObject(ofClass: objectType) {
                itemProvider.loadObject(ofClass: objectType) { object, error in
                    if let error = error {
                        print(error.localizedDescription)
                    }
                    
                    if !isLivePhoto {
                        if let image = object as? UIImage {
                            DispatchQueue.main.async {
                                self.photoPicker.mediaItems.append(item: PhotoPickerModel(with: image))
                            }
                        }
                    }
                }
            }
        }
        
        // coba load object
        private func getVideo(from itemProvider: NSItemProvider, typeIdentifier: String) {
            itemProvider.loadFileRepresentation(forTypeIdentifier: typeIdentifier) { url, error in
                if let error = error {
                    print(error.localizedDescription)
                }
                guard let url = url else { return }

                do
                {
                    let videoUrl = self.createDirectory(url: url, directoryName: "video", lowRand: 1, highRand: 99)
                    
                    try FileManager.default.copyItem(at: url, to: videoUrl)
                    
                    //CREATE NOTIF URL
                    let notifUrl = self.createDirectory(url: url, directoryName: "notif", lowRand: 100, highRand: 299)
                    try FileManager.default.copyItem(at: url, to: notifUrl)


                    DispatchQueue.main.async {
                        self.photoPicker.mediaItems.append(item: PhotoPickerModel(with: videoUrl, notifUrl: notifUrl))
                    }
                }
                catch let error
                {
                    print("An error occured \(error.localizedDescription)")
                }
            }
        }
        
        
       private func createDirectory(url:URL,directoryName: String,lowRand: Int,highRand: Int) -> URL{
            var urlDestination = URL(string: "")
           
            do {
                //  Find Application Support directory
                let fileManager = FileManager.default

                let appSupportURL = try fileManager.url(for: .documentDirectory, in: .userDomainMask, appropriateFor: nil, create: true)
                //  Create subdirectory
                let directoryURL = appSupportURL.appendingPathComponent("video")
                try fileManager.createDirectory (at: directoryURL, withIntermediateDirectories: true, attributes: nil)

                //  Create document
                let documentURL = directoryURL.appendingPathComponent ("\(Int.random(in: 1...99))\(url.lastPathComponent)")
                urlDestination = documentURL

                if FileManager.default.fileExists(atPath: documentURL.path) {
                    urlDestination = documentURL.appendingPathComponent("\(Int.random(in: lowRand...highRand))")

                }
            } catch let error{
                print("An error occured \(error.localizedDescription)")
            }
            return urlDestination ?? URL(string: "")!
        }
    }
}


extension String
{
    func replace(target: String, withString: String) -> String
       {
           return self.replacingOccurrences(of: target, with: withString, options: NSString.CompareOptions.literal, range: nil)
       }
}
