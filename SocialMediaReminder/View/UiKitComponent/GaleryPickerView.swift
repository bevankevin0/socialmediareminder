//
//  GaleryPickerView.swift
//  SocialMediaReminder
//
//  Created by bevan christian on 29/08/21.
//

import SwiftUI


struct GaleryPickerView: UIViewControllerRepresentable {
    @Environment (\.presentationMode) var presentation
    @Binding var image: UIImage?
    @Binding var videoUrl: NSURL?
    @Binding var imageData: Data?
    
    class Coordinator: NSObject, UINavigationControllerDelegate, UIImagePickerControllerDelegate {
        let parent: GaleryPickerView
        init(_ parent:GaleryPickerView) {
            self.parent = parent
        }
        
        
        func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
            if let uiImage = info[.originalImage] as? UIImage {
                parent.image = uiImage
                parent.imageData = uiImage.pngData()
                parent.videoUrl = nil
            }
            
            if let VideoUrl = info[.mediaURL] as? NSURL {
                parent.videoUrl = VideoUrl
                parent.imageData = nil
            }
            parent.presentation.wrappedValue.dismiss()
        }
    }
    func makeCoordinator() -> Coordinator {
        Coordinator(self)
    }
    
    func makeUIViewController(context: UIViewControllerRepresentableContext<GaleryPickerView>) ->  UIImagePickerController {
        let picker = UIImagePickerController()
        picker.delegate = context.coordinator
        picker.sourceType = .photoLibrary
        picker.mediaTypes = ["public.image", "public.movie"]
        return picker
    }
    
    func updateUIViewController(_ uiViewController: UIImagePickerController, context: UIViewControllerRepresentableContext<GaleryPickerView>) {

    }
}
