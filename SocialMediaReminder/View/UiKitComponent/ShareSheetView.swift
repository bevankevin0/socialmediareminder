//
//  ShareSheetView.swift
//  SocialMediaReminder
//
//  Created by bevan christian on 31/08/21.
//

import Foundation
import SwiftUI

struct ShareSheetView: UIViewControllerRepresentable {
    
    typealias Callback = (_ activityType: UIActivity.ActivityType?, _ completed: Bool, _ returnedItems: [Any]?, _ error: Error?) -> Void
    let thumbnail:UIImage?
    let contentName:String?
    let activityItems: [Any]
    let applicationActivities: [UIActivity]? = nil
    let excludedActivityTypes: [UIActivity.ActivityType]? = [.message, .addToReadingList, .mail, .openInIBooks ,.assignToContact,.markupAsPDF,.print,.airDrop]
    let callback: Callback? = nil
    
    func makeCoordinator() -> Coordinator {
        Coordinator(self)
    }
    
    class Coordinator: NSObject, UIActivityItemSource {
        
        let parent: ShareSheetView
        init(_ parent:ShareSheetView) {
            self.parent = parent
        }
        
        
        func activityViewControllerPlaceholderItem(_ activityViewController: UIActivityViewController) -> Any {
            return parent.thumbnail
        }
        
        func activityViewController(_ activityViewController: UIActivityViewController, itemForActivityType activityType: UIActivity.ActivityType?) -> Any? {
            return parent.thumbnail
        }
        
        func activityViewController(_ activityViewController: UIActivityViewController, thumbnailImageForActivityType activityType: UIActivity.ActivityType?, suggestedSize size: CGSize) -> UIImage? {
            return parent.thumbnail
        }
        
        func activityViewController(_ activityViewController: UIActivityViewController, subjectForActivityType activityType: UIActivity.ActivityType?) -> String {
            return parent.contentName ?? ""
        }

    }
    
    
    func makeUIViewController(context: Context) -> UIActivityViewController{
        
        let controller = UIActivityViewController(
            activityItems: activityItems, applicationActivities: applicationActivities)
        controller.excludedActivityTypes = excludedActivityTypes
        controller.completionWithItemsHandler = callback
        return controller
    }
    
    func updateUIViewController(_ uiViewController: UIActivityViewController, context: Context) {
        // nothing to do here
    }
}
