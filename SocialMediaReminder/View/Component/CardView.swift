//
//  CardView.swift
//  SocialMediaReminder
//
//  Created by bevan christian on 29/12/21.
//

import SwiftUI

struct CardView: View {
    var imageName: String
    var socialMediaName: String
    var announcment: String
    var listThumbnail: [UIImage] = [UIImage(named: "Instagram")!,UIImage(named: "Instagram")!,UIImage(named: "Instagram")!,UIImage(named: "Instagram")!,UIImage(named: "Instagram")!,UIImage(named: "Instagram")!,UIImage(named: "Instagram")!,UIImage(named: "Instagram")!,UIImage(named: "Instagram")!,UIImage(named: "Instagram")!,UIImage(named: "Instagram")!,UIImage(named: "Instagram")!]
    var body: some View {
        ZStack {
            Color.themes.backgroundColorThemes
            VStack(alignment:.leading) {
                HStack(spacing:10) {
                    Image("\(imageName)")
                        .resizable()
                        .frame(width: 50, height: 50)
                    Text("\(socialMediaName)")
                        .font(Font.custom("AvenirNextLTPro-Bold", size: 24))
                    Spacer()
                    Image(systemName: "arrow.right")
                        .scaledToFill()
                        .frame(width: 40, height: 50)
                    
                }
                Text("\(announcment)")
                    .font(Font.custom("AvenirNextLTPro-regular", size: 14))
                HStack {
                    if listThumbnail.count > 4 {
                        ForEach(0..<5,id:\.self) { index in
                            Image(uiImage: listThumbnail[index])
                                .resizable()
                                .frame(width: 50, height: 50)
                                .cornerRadius(10)
                        }.padding(.leading,5)
                    } else {
                        ForEach(listThumbnail,id:\.self) { gambar in
                            Image(uiImage: gambar)
                                .resizable()
                                .frame(width: 50, height: 50)
                                .cornerRadius(10)
                        }.padding(.leading,5)
                    }
                    Spacer()
                }
                .frame(width: 300)
            }
            .padding()
            .background(Color.white)
            .cornerRadius(20)
            
            
        }
    }
}

struct CardView_Previews: PreviewProvider {
    static var previews: some View {
        CardView(imageName: "Tik-Tok", socialMediaName: "Tiktok", announcment: "You have 2 content late")
    }
}
