//
//  PickerCustom.swift
//  SocialMediaReminder
//
//  Created by bevan christian on 28/12/21.
//

import SwiftUI



struct PickerCustom: View {
    @Environment(\.colorScheme) var colorScheme
    @State private var data = [SocialMediaSelectionModel(name: "Instagram", number: 0, selected: false),SocialMediaSelectionModel(name: "Facebook", number: 3, selected: false), SocialMediaSelectionModel(name: "Tik-Tok", number: 1, selected: false), SocialMediaSelectionModel(name: "Twitter", number: 2, selected: false)]
    @Binding var value: [Int]
    var socialMediaPicked: String = "instagram"
    var title: String
    var body: some View {
        VStack(alignment: .leading) {
            Text("\(title)")
                .font(Font.custom("AvenirNextLTPro-Bold", size: 14))
                .padding(.bottom,10)
            ForEach(0..<data.count) { row in
                HStack {
                    Button {
                        checkSelected(index: row)
                    } label: {
                        Text("\(data[row].name)")
                            .font(Font.custom("AvenirNextLTPro-regular", size: 14))
                            .foregroundColor(colorScheme == .dark ? Color.white : Color.black)
                        Spacer()
                        if data[row].selected {
                            Image(systemName: "checkmark.circle.fill")
                                .foregroundColor(.blue)
                        }
                    }.frame(height:30)
                }
                Divider()
            }
            .padding(.horizontal,10)
        }
        .padding([.leading,.trailing])
        .onAppear {
            if socialMediaPicked.lowercased() == "instagram" {
                data[0].selected = true
            } else if  socialMediaPicked.lowercased() == "facebook" {
                data[1].selected = true
            } else if  socialMediaPicked.lowercased() == "twitter" {
                data[3].selected = true
            } else {
                data[2].selected = true
            }
            value = data.filter { data in
                data.selected == true
            }.map({ data in
                data.number
            })
        }
    }
    
    func checkSelected(index: Int) {
        data[index].selected.toggle()
        value = data.filter { data in
            data.selected == true
        }.map({ data in
            data.number
        })
    }
}

struct PickerCustom_Previews: PreviewProvider {
    static var previews: some View {
        PickerCustom(value: .constant([1]), title: "")
    }
}
