//
//  TextFieldCustom.swift
//  SocialMediaReminder
//
//  Created by bevan christian on 28/12/21.
//

import SwiftUI

struct TextFieldCustom: View {
    @Environment(\.colorScheme) var colorScheme
    @Binding var value: String
    var title: String
    var prompt: String
    var footer: String
    var body: some View {
        VStack(alignment: .leading) {
            Text("\(title)")
                .font(Font.custom("AvenirNextLTPro-Bold", size: 14))
            TextField("\(prompt)", text: $value)
                .font(Font.custom("AvenirNextLTPro-regular", size: 13))
                .padding()
                .background(Color(UIColor(red: 0.961, green: 0.961, blue: 0.961, alpha: 1)))
                .foregroundColor(colorScheme == .dark ? Color.black : Color.black)
                .cornerRadius(12)
            Text("\(footer)")
                .font(Font.custom("AvenirNextLTPro-regular", size: 12))
        }.padding([.leading,.top,.trailing])
    }
}

struct TextFieldCustom_Previews: PreviewProvider {
    static var previews: some View {
        TextFieldCustom(value: .constant("dnejde"), title: "Title Content", prompt: "", footer: "Minimum 5 letter")
    }
}
