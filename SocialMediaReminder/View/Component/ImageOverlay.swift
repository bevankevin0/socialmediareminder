//
//  ImageOverlay.swift
//  SocialMediaReminder
//
//  Created by bevan christian on 14/09/21.
//

import SwiftUI

struct ImageOverlay: View {
    var datePassing: Date
    var isShared: Bool
    let dateFormatter = DateFormatter()
    var todayDateFormated: String {
        dateFormatter.dateFormat = "MMM dd,yyyy"
        return dateFormatter.string(from: Date())
    }
    var formatedDate: String {
        dateFormatter.dateFormat = "MMM dd,yyyy"
        return dateFormatter.string(from: datePassing)
    }
    var isLate2: String {
        if dateFormatter.date(from: todayDateFormated)! > dateFormatter.date(from: formatedDate)! && isShared == false {
            // telat dan belum dishare
            return "Late"
        } else if dateFormatter.date(from: todayDateFormated)! == dateFormatter.date(from: formatedDate)! && isShared == false {
            // sama namun belom dishare
            return "Today"
        } else if dateFormatter.date(from: todayDateFormated)! < dateFormatter.date(from: formatedDate)! && isShared == false {
            // belum telat dan belom dishared
            return "On Scheduled"
        } else {
            return "Shared"
        }
    }
    var body: some View {
        VStack(alignment:.trailing) {
            Text("\(formatedDate)")
                .font(Font.custom("AvenirNextLTPro-regular", size: 11))
                .foregroundColor(.black)
                .fontWeight(.heavy)
            Text("\(isLate2)")
                .font(Font.custom("AvenirNextLTPro-regular", size: 11))
                .fontWeight(.heavy)
                .foregroundColor(.black)
        }
        .padding(3)
        .background(Color.white)
        .opacity(0.45)
        .cornerRadius(3)
    }
}

struct ImageOverlay_Previews: PreviewProvider {
    static var previews: some View {
        ImageOverlay(datePassing: Date(), isShared: false)
    }
}
