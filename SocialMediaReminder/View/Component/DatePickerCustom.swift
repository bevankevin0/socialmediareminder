//
//  PostingPickerCustom.swift
//  SocialMediaReminder
//
//  Created by bevan christian on 28/12/21.
//

import SwiftUI

struct DatePickerCustom: View {
    @Binding var value: Date
    var title: String
    var tomorrow: Date {
        return Calendar.current.date(byAdding: .day, value: 0, to: Date())!
    }
    var body: some View {
        VStack(alignment: .leading) {
            Text("\(title)")
                .font(Font.custom("AvenirNextLTPro-Bold", size: 14))
            DatePicker("Posting Schedule", selection: $value, in: tomorrow..., displayedComponents: [.date, .hourAndMinute])
                .font(Font.custom("AvenirNextLTPro-regular", size: 13))
        }.padding([.leading,.top,.trailing])
    }
}

struct PostingPickerCustom_Previews: PreviewProvider {
    static var previews: some View {
        DatePickerCustom(value: .constant(Date()), title: "Pick Your Schedule")
    }
}
