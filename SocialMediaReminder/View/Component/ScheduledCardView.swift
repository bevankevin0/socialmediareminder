//
//  ScheduledCardComponent.swift
//  SocialMediaReminder
//
//  Created by bevan christian on 11/01/22.
//

import SwiftUI

struct ScheduledCardView: View {
    @Environment(\.colorScheme) var colorScheme
    var datePassing: Date
    var title: String
    var image: UIImage
    var socialMediaType: String
    let dateFormatter = DateFormatter()
    var formatedDate: String {
        dateFormatter.dateFormat = "MMM dd,yyyy HH:mm"
        return dateFormatter.string(from: datePassing)
    }
    var body: some View {
        VStack {
            HStack {
                VStack(alignment:.leading,spacing: 10) {
                    Text("\(formatedDate)")
                        .font(Font.custom("AvenirNextLTPro-lt", size: 12))
                        .fontWeight(.semibold)
                        
                    Text("\(title)")
                        .font(Font.custom("AvenirNextLTPro-Bold", size: 14))
                    Text("\(socialMediaType)")
                        .font(Font.custom("AvenirNextLTPro-regular", size: 12))
                        .foregroundColor(.gray)
                    
                    
                }
                Spacer()
                Image(uiImage: image)
                    .resizable()
                    .frame(width: 72, height: 72)
                    .cornerRadius(8)
            }
            .foregroundColor(colorScheme == .dark ? Color.black : Color.black)
            .padding()
        }
        
    }
}

struct ScheduledCardComponent_Previews: PreviewProvider {
    static var previews: some View {
        ScheduledCardView(datePassing: Date(), title: "12121", image: UIImage(named: "Nature") ?? UIImage(), socialMediaType: "tiktok")
    }
}
