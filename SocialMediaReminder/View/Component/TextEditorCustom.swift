//
//  TextEditorCustom.swift
//  SocialMediaReminder
//
//  Created by bevan christian on 28/12/21.
//

import SwiftUI

struct TextEditorCustom: View {
    @Environment(\.colorScheme) var colorScheme
    @Binding var value: String
    var title: String
    var prompt: String
    var footer: String
  

    var body: some View {
        VStack(alignment: .leading) {
            Text("\(title)")
                .font(Font.custom("AvenirNextLTPro-Bold", size: 14))
            TextEditor(text: $value)
                .font(Font.custom("AvenirNextLTPro-regular", size: 13))
                .background(colorScheme == .dark ? Color(UIColor(red: 0.961, green: 0.961, blue: 0.961, alpha: 1)): Color(UIColor(red: 0.961, green: 0.961, blue: 0.961, alpha: 1)))
                .foregroundColor(colorScheme == .dark ? Color.black : Color.black)
                .frame(height: 120)
                .cornerRadius(12)
                
            Text("\(footer)")
                .font(Font.custom("AvenirNextLTPro-regular", size: 12))
        }.padding([.leading,.top,.trailing])
    }
}

struct TextEditorCustom_Previews: PreviewProvider {
    static var previews: some View {
        TextEditorCustom(value: .constant("hello"), title: "title", prompt: "prompt", footer: "minimum 5 letter")
    }
}
