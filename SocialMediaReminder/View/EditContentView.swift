//
//  EditContentView.swift
//  SocialMediaReminder
//
//  Created by bevan christian on 15/09/21.
//

import SwiftUI

struct EditContentView: View {
    @Environment(\.colorScheme) var colorScheme
    @Environment (\.presentationMode) var presented
    @State var title: String
    @State var caption:String
    @State var hastag:String
    @State var dateSelection: Date
    @State var uidNotif: String
    @State var socialMediaType:String
    @State var editDone = false
    @State var kontent:TemporaryContent
    @ObservedObject var detailVm:DetailVM
    var body: some View {
        NavigationView {
            GeometryReader { geo in
                ZStack {
                    VStack {
                        Form(content: {
                            Section(header:Text("Caption")) {
                                TextEditor(text: $caption)
                                .frame(height:geo.size.height * 0.2)
                            }
                            Section(header:Text("Hastag")) {
                                TextEditor(text: $hastag)
                                .frame(height:geo.size.height * 0.2)
                            }
                            if uidNotif != "0" {
                                Section(header:Text("Posting Time")) {
                                    DatePicker("Posting Schedule", selection: $dateSelection, in: tomorrow..., displayedComponents: [.date,.hourAndMinute])
                                }
                            } else {
                                Text("Cannot change schedule created before version 1.2 update. But you can change the caption")
                                    .font(.caption)
                                    .foregroundColor(.gray)
                            }
                        })
                        .padding(.top,30)
                        .frame(height:geo.size.height * 0.9)
                        ZStack {
                            colorScheme == .light ? Color.themes.navbarLight : Color.themes.primaryColor
                            Button(action: {
                                kontent.caption = caption
                                kontent.scheduledPost = dateSelection
                                kontent.hastag = hastag
                                print("konten update \(kontent)")
                                detailVm.updateKonten(dateSelection, uidNotif: uidNotif, konten: kontent)
                                editDone = true
                            }, label: {
                                Text("Done")
                                    .frame(width: geo.size.width * 0.8 , height: 40, alignment: .center)
                                    .background(Color.green)
                                    .foregroundColor(.white)
                                    .cornerRadius(10)
                                    .offset(y:-20)
                            }).padding(.bottom,20)
                        }
                    }.alert(isPresented: $editDone) {
                        Alert(title: Text("Done"), message: Text("Your changes have been saved"), dismissButton: .cancel(Text("Ok"), action: {
                            presented.wrappedValue.dismiss()
                        }))
                    }
                }.ignoresSafeArea(.all)
           
            }
            .navigationTitle("Edit Detail")
            .navigationBarTitleDisplayMode(.inline)
        }
    
    }
}

