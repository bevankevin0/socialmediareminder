//
//  GenerateDataOnboarding.swift
//  SocialMediaReminder
//
//  Created by bevan christian on 13/09/21.
//

import Foundation


struct GenerateDataOnboarding {
    var data = [OnboardingModel]()
    
    init() {
        data.append(OnboardingModel(imageName: "notification", title: "Active Notification", description: "Content Reminder always set notification automaticlly when you scheduled to post a content & it will appear on the day you schedule it to be posted"))
        data.append(OnboardingModel(imageName: "clipboard", title: "Integrate with Clipboard", description: "When you share the content,Content Reminder automatically copies your caption to the clipboard so you can paste on anywhere"))
        data.append(OnboardingModel(imageName: "schedule", title: "Schedule at Anytime", description: "Schedule you content anytime & Content Reminder always keep track your schedule"))
    }
}
