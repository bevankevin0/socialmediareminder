//
//  OnboardingTabView.swift
//  SocialMediaReminder
//
//  Created by bevan christian on 13/09/21.
//

import SwiftUI

struct OnboardingTabView: View {
    var data = GenerateDataOnboarding()
    @Binding var isOldUser:Bool
    @State var selection = 0
    var body: some View {
        VStack {
            TabView(selection: $selection) {
                ForEach(data.data) { row in
                    OnboardingView(title: row.title, description: row.description, image: row.imageName)
                }
                
            }
            .tabViewStyle(PageTabViewStyle())
            .indexViewStyle(PageIndexViewStyle(backgroundDisplayMode: .always))
            Button(action: {
                isOldUser = true
                UserDefaults.standard.setValue(true, forKey: "oldUser")
            }, label: {
                Text("Get Started")
                    .frame(width: 300, height: 40, alignment: .center)
                    .background(Color.green)
                    .foregroundColor(.white)
                    .cornerRadius(10)
            }).padding()
        }
    }
}






struct OnboardingTabView_Previews: PreviewProvider {
    static var previews: some View {
        OnboardingTabView(isOldUser: .constant(false))
    }
}
