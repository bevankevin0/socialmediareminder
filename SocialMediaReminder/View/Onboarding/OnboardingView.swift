//
//  OnboardingView.swift
//  SocialMediaReminder
//
//  Created by bevan christian on 13/09/21.
//

import SwiftUI

struct OnboardingView: View {
    var title:String
    var description:String
    var image:String
    var body: some View {
        VStack() {
            Image("\(image)")
                .resizable()
                .frame(width: 300, height: 300, alignment: .center)
                .padding(.bottom,20)
            Text("\(title)")
                .font(.title2)
                .bold()
            Text("\(description)")
                .font(.body)
                .foregroundColor(.gray)
                .multilineTextAlignment(.center)
                .padding([.top, .leading, .trailing])
            
        }
    }
}

struct OnboardingView_Previews: PreviewProvider {
    static var previews: some View {
        OnboardingView(title: "JUDUL", description: "DESKRIPSIDESKRIPSIDESKRIPSIDESKRIPSIDESKRIPSIDESKRIPSIDESKRIPSIDESKRIPSIDESKRIPSIDESKRIPSIDESKRIPSIDESKRIPSIDESKRIPSI", image: "Nature")
    }
}
