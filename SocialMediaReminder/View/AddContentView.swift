//
//  AddContentView.swift
//  SocialMediaReminder
//
//  Created by bevan christian on 28/08/21.
//

import SwiftUI
import AVKit
import PhotosUI

struct AddContentView: View {
    @Environment(\.colorScheme) var colorScheme
    @Environment (\.presentationMode) var presented
    var socialMediaPicked: String = "instagram"
    @State var showGaleryPicker = false
    @State var finishAdd = false
    @State var imagePlaceholder: UIImage?
    @State var isLoading = false
    @State var disableButton = false
    @StateObject var addContentVm = AddContentVM()
    let dates = Date()
    var tomorrow: Date {
        return Calendar.current.date(byAdding: .day, value: 1, to: Date())!
    }
    var body: some View {
        NavigationView {
            GeometryReader { geo in
                ZStack {
                    //Color.themes.addColor
                    if isLoading {
                        ProgressView()
                            .zIndex(1)
                    }
                    VStack {
                        Spacer()
                        ScrollView {
                            VStack(alignment:.leading) {
                                HStack() {
                                    Spacer()
                                    if addContentVm.itemsContentPicOrVid.mediaType == .photo {
                                        Button {
                                            showGaleryPicker = true
                                        } label: {
                                            Image(uiImage: (addContentVm.itemsContentPicOrVid.photo ?? UIImage(named: "addImagePlaceholder"))!)
                                                .resizable()
                                                .scaledToFit()
                                                .frame(width: geo.size.width * 0.8, height: 200)
                                                .background(Color(UIColor(red: 0.961, green: 0.961, blue: 0.961, alpha: 1)))
                                                .cornerRadius(10)
                                                
                                            
                                        }
                                    } else if addContentVm.itemsContentPicOrVid.mediaType == .video {
                                        if let url = addContentVm.itemsContentPicOrVid.url {
                                            VideoPlayer(player: AVPlayer(playerItem: AVPlayerItem(url: url)))
                                                .frame(width: geo.size.width * 0.8, height: 200)
                                        } else { EmptyView() }
                                    }
                                    Spacer()
                                }
                                
                                TextFieldCustom(value: $addContentVm.postTitle, title: "Content Title", prompt: "Title", footer: "Minimum 5 letters")
                                TextEditorCustom(value: $addContentVm.captionContent, title: "Caption", prompt: "Input Your Caption", footer: "Minimum 5 letters")
                                DatePickerCustom(value: $addContentVm.datePosting, title: "Pick Your Schedule")
                                VStack {
                                    TextEditorCustom(value: $addContentVm.hastag, title: "Hastag", prompt: "Hastag", footer: "")
                                    HStack {
                                        Text("Accuracy :")
                                        if addContentVm.accuracy == "0" {
                                            Text("Hastag Not Found")
                                        } else {
                                            Text("\(addContentVm.accuracy)% Based On MobileNetV2 Model")
                                        }
                                        
                                        Spacer()
                                    }
                                    .font(Font.custom("AvenirNextLTPro-regular", size: 12))
                                    .padding(.bottom)
                                    .padding(.horizontal)
                                   
                                }
                                PickerCustom(value: $addContentVm.socialMediaType, socialMediaPicked: socialMediaPicked, title: "Social Media")
                                    .padding(.bottom,100)
                                   

                            }
                        }
                        .frame(height:geo.size.height * 0.9)
                        
                    }
                    .frame(height:geo.size.height * 1)
                    .alert(isPresented: $finishAdd) {
                        return Alert(title: Text("Added"), message: Text("Your content has been successfully added and a notification will appear on the scheduled day"), dismissButton: .cancel(Text("Ok"), action: {
                            disableButton = false
                            presented.wrappedValue.dismiss()
                        }))
                    }
                    .navigationBarItems(trailing: Button(action: {
                        if disableButton == false {
                            isLoading = true
                            disableButton = true
                            addContentVm.insertContent {
                                DispatchQueue.main.async {
                                    isLoading = false
                                    finishAdd = true
                                }
                            }
                        }
                    }, label: {
                        Text("Add")
                            .foregroundColor(addContentVm.isInvalid ? Color.gray : Color.blue)
                            .disabled(disableButton)
                    }).disabled(addContentVm.isInvalid))
                }
                
            }.ignoresSafeArea(.all)
                .navigationTitle("Add Content Page")
                .navigationBarTitleDisplayMode(.inline)
                .onAppear(perform: {
                }).sheet(isPresented: $showGaleryPicker, content: {
                    PHPickerView(didFinishPicking: { didSelectItem in
                        showGaleryPicker = false
                    }, mediaItems: addContentVm)
                })
        }.ignoresSafeArea(.all)
    }
}

//
//struct AddContentView_Previews: PreviewProvider {
//    static var previews: some View {
//        AddContentView()
//    }
//}
