//
//  NotificationService.swift
//  SocialMediaReminder
//
//  Created by bevan christian on 31/08/21.
//

import Foundation
import NotificationCenter


class NotificationService: NotificationProtocol {
    static let shared = NotificationService()
    private let notificationCenter = UNUserNotificationCenter.current()
    
    init() {
        requestAuthorization()
    }
    
    func requestAuthorization() {
        notificationCenter.requestAuthorization(options: [.sound, .alert, .badge, .announcement, .criticalAlert]) { granted, error in
            if error == nil {
                DispatchQueue.main.async { 
                UIApplication.shared.registerForRemoteNotifications()
                }
            }
        }
    }
    
    func createNotification(title:String, date:Date, caption:String, socialType:String,url:String,counter: Double, completion:@escaping(String)->Void) {
        let content = creatingContentNotification(title: title, socialType: socialType, url: url)
        let request = creatingRequest(content: content, date: date,counter: counter)
        notificationCenter.add(request) { (error) in
            if error != nil {
                print(error?.localizedDescription as Any)
            }
            completion(request.identifier)
        }
    }
   
    func cancelOldNotification(uid:String) {
        notificationCenter.removePendingNotificationRequests(withIdentifiers: [uid])
    }
    
}



extension NotificationService {
    private func creatingContentNotification(title: String,socialType: String,url: String) -> UNMutableNotificationContent {
        let content = UNMutableNotificationContent()
        content.title = "Hello don't forget post \(title)"
        content.body = "Hello, we want to remind you that it's time for your content called \(title) to be posted in \(socialType). Good luck"
        content.sound = UNNotificationSound.defaultCritical
        content.badge = 1
        print("content image notif \(url)")
        if url != "" {
            do {
                let attachment = try UNNotificationAttachment(identifier: title, url: URL(string: url)!, options: nil)
                content.attachments = [attachment]
            } catch let error{
                print("error notif \(error.localizedDescription)")
            }
        }

        return content
    }
    private func creatingRequest(content:UNMutableNotificationContent,date:Date,counter: Double = 0) -> UNNotificationRequest {
        let triggerDate = Calendar.current.dateComponents([.year, .month, .day, .hour, .minute, .second, .timeZone],
                                                          from: date.addingTimeInterval(counter))
        print("interval date \(triggerDate) dan \(date.addingTimeInterval(counter))")
        let trigger = UNCalendarNotificationTrigger(dateMatching: triggerDate, repeats: true)
        let uuidString = UUID().uuidString
        let request = UNNotificationRequest(identifier: uuidString,
                                            content: content,
                                            trigger: trigger)
        return request
    }
}

