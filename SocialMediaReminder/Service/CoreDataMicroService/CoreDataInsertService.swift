//
//  CoreDataInsertService.swift
//  SocialMediaReminder
//
//  Created by bevan christian on 16/09/21.
//

import Foundation
import Combine
import CoreData

protocol CoreDataInsertProtocol {
    func insertKonten(kontenNew: TemporaryContent)
}


class CoreDataInsertService: CoreDataInsertProtocol {
    func insertKonten(kontenNew: TemporaryContent) {
        let coreService = CoreDataService.shared
        let context = coreService.container.viewContext
        context.performAndWait {
            insertVariabelToModel(context: context, kontenNew: kontenNew)
        }
        do {
            try context.save()
            print("saved")
        } catch  {
            print("error saved")
        }
        
    }
    
    private func insertVariabelToModel(context:NSManagedObjectContext,kontenNew: TemporaryContent) {
        let konten = Konten.init(context: context)
        konten.idKonten = kontenNew.id
        konten.contentName = kontenNew.name
        konten.contentCaption = kontenNew.caption
        konten.imageData = kontenNew.imageData
        konten.isShared = kontenNew.isShared ?? false
        konten.scheduledPostTime = kontenNew.scheduledPost
        konten.socialMediaType = kontenNew.socialMediaType
        konten.videoUrl = kontenNew.videoUrl
        konten.notifUid = kontenNew.notifUid
        konten.hastag = kontenNew.hastag
        konten.videoUrlNotif = kontenNew.videoUrlNotif
        konten.uploadDate = Date()
    }
}
