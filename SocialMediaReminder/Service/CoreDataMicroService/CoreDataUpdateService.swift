//
//  CoreDataUpdateService.swift
//  SocialMediaReminder
//
//  Created by bevan christian on 16/09/21.
//

import Foundation
import CoreData
import Combine

protocol CoreDataUpdateProtocol {
    func updateContent (_ konten: TemporaryContent)
    func updateContentShare (_ konten: TemporaryContent)
}


class CoreDataUpdateService: CoreDataUpdateProtocol {

    func updateContent (_ konten: TemporaryContent) {
        let coreService = CoreDataService.shared
        let context = coreService.container.viewContext
        context.automaticallyMergesChangesFromParent = true
        context.performAndWait {
            let request: NSFetchRequest<Konten> = Konten.fetchRequest()
            request.predicate = NSPredicate(format: "\(#keyPath(Konten.idKonten)) == %@", konten.id)
            if let yangMauDiUpdate = try? context.fetch(request), let member = yangMauDiUpdate.first {
                member.contentCaption = konten.caption
                member.contentName = konten.name
                member.isShared = konten.isShared ?? false
                member.notifUid = konten.caption
                member.scheduledPostTime = konten.scheduledPost
                member.hastag = konten.hastag
                do {
                    try context.save()
                    print("saved")
                } catch  {
                    print("error saved")
                }
            }
        }
    }
    
    func updateContentShare (_ konten: TemporaryContent) {
        let coreService = CoreDataService.shared
        let context = coreService.container.viewContext
        context.automaticallyMergesChangesFromParent = true
        context.performAndWait {
            let request: NSFetchRequest<Konten> = Konten.fetchRequest()
            request.predicate = NSPredicate(format: "\(#keyPath(Konten.idKonten)) == %@", konten.id)
            if let yangMauDiUpdate = try? context.fetch(request), let member = yangMauDiUpdate.first {
                member.contentCaption = konten.caption
                member.contentName = konten.name
                member.isShared = true
                member.notifUid = konten.caption
                member.scheduledPostTime = konten.scheduledPost
                member.hastag = konten.hastag
                do {
                    try context.save()
                    print("saved")
                } catch  {
                    print("error saved")
                }
            }
        }
    }
}
