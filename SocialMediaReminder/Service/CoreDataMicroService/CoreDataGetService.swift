//
//  CoreDataGetService.swift
//  SocialMediaReminder
//
//  Created by bevan christian on 17/09/21.
//

import Foundation
import Combine
import CoreData

protocol CoreDataGetProtocol {
    func getAllContentBySocialMedia(socialMedia:String) -> Future<[Konten],errorCoreData>
    func getFilterContentByLateness(isShared:Bool,isLate:formatPredicate) -> Future<[Konten],errorCoreData>
    func getFilterContentByLatenessBySocialMedia(isShared:Bool,isLate:formatPredicate,socialMedia: String) -> Future<[Konten],errorCoreData>
    func getAllContentToday() -> Future<[Konten],errorCoreData>
}

class CoreDataGetService: CoreDataGetProtocol {
    var coreService: CoreDataService!
    
    func getAllContentBySocialMedia(socialMedia:String) -> Future<[Konten],errorCoreData> {
        coreService = CoreDataService.shared
        return Future() { [self] promise in
            let context = coreService.container.viewContext
            context.automaticallyMergesChangesFromParent = true
            context.performAndWait {
                let request:NSFetchRequest<Konten> = createPredicateforAllSocialMedia(socialMedia: socialMedia)
                do {
                    let result = try context.fetch(request)
                    
                    promise(Result.success(result))
                } catch  {
                    promise(.failure(errorCoreData.contentEmpty))
                }
            }
        }
    }

    func getFilterContentByLateness(isShared:Bool,isLate:formatPredicate) -> Future<[Konten],errorCoreData> {
        coreService = CoreDataService.shared
        return Future() { [self] promise in
            let context = coreService.container.viewContext
            context.perform {
                let request:NSFetchRequest<Konten> = creatingPredicateforReminder(isAlreadyShared: false, formatPredicate: isLate.rawValue) // belum di share dan jadwal sudah lewat
                do {
                    var result = try context.fetch(request)
                    if isLate == .notLate {
                         result = result.filter { data in
                             print(data.id)
                             let currentComponent = Calendar.current.dateComponents([.day, .month], from: Date())
                             let components = Calendar.current.dateComponents([.day, .month], from: data.scheduledPostTime ?? Date())
                             let currentDay = currentComponent.day ?? 0
                             let currentMonth = currentComponent.month ?? 0
                             let day = components.day ?? 0
                             let month = components.month ?? 0
                             if currentMonth < month {
                                 // masih bulan depan
                                 return true
                             } else if currentMonth == month {
                                 if currentDay < day {
                                     // masih besok besok namun dalam bulan yang sama
                                     return true
                                 } else {
                                     // sudah lewat hari posting e
                                     return false
                                 }
                             } else {
                                 // sudah lewat bulane
                                return false
                             }
                        }
                    }
                    promise(Result.success(result))
                } catch  {
                    promise(.failure(errorCoreData.contentEmpty))
                }
            }
        }
    }
    
    func getAllContentToday() -> Future<[Konten],errorCoreData> {
        coreService = CoreDataService.shared
        return Future() { [self] promise in
            let context = coreService.container.viewContext
            context.perform {
                let request:NSFetchRequest<Konten> = creatingPredicateForAllContentToday()
                do {
                    var result = try context.fetch(request)
                    
                    result = result.filter { data in
                        let currentComponent = Calendar.current.dateComponents([.day, .month, .year], from: Date())
                        let components = Calendar.current.dateComponents([.day, .month, .year], from: data.scheduledPostTime ?? Date())
                        let currentDay = currentComponent.day ?? 0
                        let currentMonth = currentComponent.month ?? 0
                        let currentYear = currentComponent.year ?? 0
                        let day = components.day ?? 0
                        let month = components.month ?? 0
                        let year = components.year ?? 0
                        if (currentDay == day) && (month == currentMonth) && (year == currentYear) {
                            return true
                        } else {
                            return false
                        }
                    }
                    promise(Result.success(result))
                } catch  {
                    promise(.failure(errorCoreData.contentEmpty))
                }
            }
        }
    }
    
    // get lateness per social media for card homepage
    func getFilterContentByLatenessBySocialMedia(isShared:Bool,isLate:formatPredicate,socialMedia: String) -> Future<[Konten],errorCoreData> {
        coreService = CoreDataService.shared
        return Future() { [self] promise in
            let context = coreService.container.viewContext
            context.perform {
                let request:NSFetchRequest<Konten> = creatingPredicateForLatePerSocialMedia(isAlreadyShared: false, formatPredicate: isLate.rawValue,socialMedia: socialMedia)
                do {
                    let result = try context.fetch(request)
                    promise(Result.success(result))
                } catch  {
                    promise(.failure(errorCoreData.contentEmpty))
                }
            }
        }
    }
}

extension CoreDataGetService {
    private func creatingPredicateforReminder(isAlreadyShared: Bool, formatPredicate: String) ->NSFetchRequest<Konten> {
        let request:NSFetchRequest<Konten> = Konten.fetchRequest()
        let isSharedPredicate = NSPredicate(format: "\(#keyPath(Konten.isShared)) == %@", NSNumber(value: isAlreadyShared))
        let today = Date()
        let datePredicate = NSPredicate(format: formatPredicate, #keyPath(Konten.scheduledPostTime), today as CVarArg)
        let andPredicate = NSCompoundPredicate(type: .and, subpredicates: [isSharedPredicate, datePredicate])
        request.returnsObjectsAsFaults = false
        request.predicate = andPredicate
        return request
    }
    
    private func creatingPredicateForAllContentToday() -> NSFetchRequest<Konten> {
        let request:NSFetchRequest<Konten> = Konten.fetchRequest()
        let today = Date()
        let datePredicate = NSPredicate(format: formatPredicate.today.rawValue, #keyPath(Konten.scheduledPostTime), today as CVarArg)
        request.returnsObjectsAsFaults = false
        request.predicate = datePredicate
        return request
    }
    
    private func creatingPredicateForLatePerSocialMedia(isAlreadyShared: Bool,formatPredicate: String,socialMedia: String) ->NSFetchRequest<Konten> {
        let request:NSFetchRequest<Konten> = Konten.fetchRequest()
        let isSharedPredicate = NSPredicate(format: "\(#keyPath(Konten.isShared)) == %@", NSNumber(value: isAlreadyShared))
        let today = Date()
        let datePredicate = NSPredicate(format: formatPredicate, #keyPath(Konten.scheduledPostTime), today as CVarArg)
        let socialMediaPredicate = NSPredicate(format: "\(#keyPath(Konten.socialMediaType)) == %@", socialMedia)
        let andPredicate = NSCompoundPredicate(type: .and, subpredicates: [isSharedPredicate, datePredicate,socialMediaPredicate])
        request.returnsObjectsAsFaults = false
        request.predicate = andPredicate
        return request
    }
    
    
    private func createPredicateforAllSocialMedia(socialMedia:String) -> NSFetchRequest<Konten> {
        let request:NSFetchRequest<Konten> = Konten.fetchRequest()
        let predicate = NSPredicate(format: "\(#keyPath(Konten.socialMediaType)) == %@", socialMedia)
        let sort = NSSortDescriptor(key: "uploadDate", ascending: false)
        request.returnsObjectsAsFaults = false
        request.predicate = predicate
        request.sortDescriptors = [sort]
        return request
    }
}
