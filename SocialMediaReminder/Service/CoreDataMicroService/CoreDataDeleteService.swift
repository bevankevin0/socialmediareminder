//
//  CoreDataDeleteService.swift
//  SocialMediaReminder
//
//  Created by bevan christian on 16/09/21.
//

import Foundation
import CoreData
import Combine



protocol CoreDataDeleteProtocol {
    func delete(_ name:String,completion:@escaping(Bool)->Void)
}

class CoreDataDeleteService: CoreDataDeleteProtocol {
   
//    func delete(_ name:String,completion:@escaping(Bool)->Void){
//            let coreService = CoreDataService.shared
//            let context = coreService.container.viewContext
//            let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: "Konten")
//            fetchRequest.predicate = NSPredicate(format: "\(#keyPath(Konten.contentName)) == %@", name)
//            let delete = NSBatchDeleteRequest(fetchRequest: fetchRequest)
//            delete.resultType = .resultTypeCount
//            if let delete = try? context.execute(delete) as? NSBatchDeleteResult {
//                if delete.result != nil {
//                    completion(true)
//                } else {
//                    completion(false)
//                }
//            }
//    }
    
    func delete(_ id:String,completion:@escaping(Bool)->Void){
            let coreService = CoreDataService.shared
            let context = coreService.container.viewContext
            let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: "Konten")
            fetchRequest.predicate = NSPredicate(format: "\(#keyPath(Konten.idKonten)) == %@", id)
            let delete = NSBatchDeleteRequest(fetchRequest: fetchRequest)
            delete.resultType = .resultTypeCount
            if let delete = try? context.execute(delete) as? NSBatchDeleteResult {
                if delete.result != nil {
                    completion(true)
                } else {
                    completion(false)
                }
            }
    }
}
