//
//  ThumbnailService.swift
//  SocialMediaReminder
//
//  Created by bevan christian on 29/08/21.
//

import Foundation
import AVFoundation
import SwiftUI
import Combine
import PhotosUI

class ThumbnailService {
//    static let shared = ThumbnailService()
    var subscription = Set<AnyCancellable>()
    var kontenToAppend = [TemporaryContent]()
    var dataFinalThumbnail = PassthroughSubject<[TemporaryContent],Never>()
    
    init() {
        requestAcsess()
    }
    
    func requestAcsess() {
        PHPhotoLibrary.requestAuthorization(for: .readWrite) { (status) in
            DispatchQueue.main.async {
                print(status)
            }
        }
    }
        
        
        func makeThumbNail(kontenData:[Konten])  {
            kontenToAppend.removeAll()
            for data in kontenData {
                if data.imageData == nil {
                    let video = getThumbnailFromVideo(url: data.videoUrl ?? "")
                    print("video thumbnail \(data.videoUrlNotif)")
                    kontenToAppend.append(TemporaryContent(
                        id: data.idKonten ?? "", name: data.contentName,
                                            caption: data.contentCaption,
                                            isShared: data.isShared,
                                            scheduledPost: data.scheduledPostTime,
                                            socialMediaType: data.socialMediaType,
                                            imageData: data.imageData,
                        videoUrl: data.videoUrl, videoUrlNotif: data.videoUrlNotif,
                                            thumbnail: video, uploadDate: data.uploadDate,notifUid: data.notifUid,hastag: data.hastag ?? "-"))
                    
                } else {
                    let hasil = getThumbnailFromImage(url: data.imageData!)
                    kontenToAppend.append(TemporaryContent(id: data.idKonten ?? "", name: data.contentName,
                                                           caption: data.contentCaption,
                                                           isShared: data.isShared,
                                                           scheduledPost: data.scheduledPostTime,
                                                           socialMediaType: data.socialMediaType,
                                                           imageData: data.imageData,
                                                           videoUrl: data.videoUrl, videoUrlNotif: data.videoUrlNotif,
                                                           thumbnail: hasil,uploadDate: data.uploadDate,notifUid: data.notifUid,hastag: data.hastag ?? "-"))
                    
                }
                
            }
            if kontenToAppend.isEmpty == false {
                dataFinalThumbnail.send(kontenToAppend)
            }
           
        }
        
        func getThumbnailFromVideo(url:String) -> UIImage? {
            guard let urlFix = URL(string: url) else { return UIImage()}
            let asset = AVAsset(url: URL(string: url)!)
            let avAssetImageGenerator = AVAssetImageGenerator(asset: asset)
            avAssetImageGenerator.appliesPreferredTrackTransform = true
            let thumnailTime = CMTimeMake(value: 1, timescale: 1)
            do {
                let cgThumbImage = try avAssetImageGenerator.copyCGImage(at: thumnailTime, actualTime: nil)
                let thumbImage = UIImage(cgImage: cgThumbImage)
                return thumbImage
            } catch let error {
                print(error)
            }
            return nil
        }
        
        func getThumbnailFromImage(url:Data) -> UIImage {
            let image = UIImage(data: url)!
            return image
        }
    }


