//
//  CoreDataService.swift
//  SocialMediaReminder
//
//  Created by bevan christian on 28/08/21.
//

import Foundation
import CoreData
import Combine

class CoreDataService: CoreDataProtocol {
    static var shared = CoreDataService()
    private var coreDataDeleteService: CoreDataDeleteProtocol!
    private var coreDataUpdateService: CoreDataUpdateProtocol!
    private var coreDataInsertService: CoreDataInsertProtocol!
    private var coreDataGetService: CoreDataGetProtocol!
    private var subscription = Set<AnyCancellable>()
    var container: NSPersistentContainer!
   
    
    init(coreDeleteService:CoreDataDeleteProtocol = CoreDataDeleteService(),coreUpdateService: CoreDataUpdateProtocol = CoreDataUpdateService()
         ,coreInsertService: CoreDataInsertProtocol = CoreDataInsertService(), coreDataGetService: CoreDataGetProtocol = CoreDataGetService()) {
        self.createPeristenceContainer()
        self.insertingDependency(coreDataGetService, coreUpdateService, coreDeleteService, coreInsertService)
    }
    
    private func createContext() -> NSManagedObjectContext {
        let taskContext = container.newBackgroundContext()
        return taskContext
    }
    
     // MARK: GET METHOD
    func getAllContentBySocialMedia(socialMedia: String) -> Future<[Konten],errorCoreData> {
        return Future() { [weak self] promise in
            self?.coreDataGetService.getAllContentBySocialMedia(socialMedia: socialMedia)
                .sink { completion in
                    switch completion {
                    case .finished:
                        print("sukses")
                    case .failure(_):
                        promise(.failure(errorCoreData.contentEmpty))
                    }
                } receiveValue: { allContent in
                    promise(.success(allContent))
                }.store(in: &self!.subscription)
        }
    }
    
    func getFilterContentByLateness(isShared: Bool, isLate: formatPredicate) -> Future<[Konten],errorCoreData> {
        return Future() { [weak self] promise in
            self?.coreDataGetService.getFilterContentByLateness(isShared: isShared, isLate: isLate)
                .sink { completion in
                    switch completion {
                    case .finished:
                        print("sukses")
                    case .failure(_):
                        promise(.failure(errorCoreData.contentEmpty))
                    }
                } receiveValue: { allContent in
                    promise(.success(allContent))
                }.store(in: &self!.subscription)
        }
    }
    
    func getAllContentToday() -> Future<[Konten],errorCoreData> {
        return Future() { [weak self] promise in
            self?.coreDataGetService.getAllContentToday()
                .sink { completion in
                    switch completion {
                    case .finished:
                        print("sukses")
                    case .failure(_):
                        promise(.failure(errorCoreData.contentEmpty))
                    }
                } receiveValue: { allContent in
                    promise(.success(allContent))
                }.store(in: &self!.subscription)
        }
    }
    
    func getLateContentPerSocialMedia(socialMedia: String) -> Future<[Konten],errorCoreData> {
        return Future() { [weak self] promise in
            self?.coreDataGetService.getFilterContentByLatenessBySocialMedia(isShared: false, isLate: .late, socialMedia: socialMedia)
                .sink { completion in
                    switch completion {
                    case .finished:
                        print("sukses")
                    case .failure(_):
                        promise(.failure(errorCoreData.contentEmpty))
                    }
                } receiveValue: { allContent in
                    promise(.success(allContent))
                }.store(in: &self!.subscription)
        }
        
    }
        
    // MARK: INSERT METHOD
    func insertContent(newContent: TemporaryContent) {
        coreDataInsertService.insertKonten(kontenNew: newContent)
    }
    
    // MARK: UPDATE METHOD
    func updateContent(_ konten: TemporaryContent) {
        coreDataUpdateService.updateContent(konten)
    }
    
    func updateShared(_ konten: TemporaryContent) {
        coreDataUpdateService.updateContentShare(konten)
    }
    
    // MARK: DELETE METHOD
    func deleteContent(_ contentName:String) -> Future<Bool,errorCoreData> {
        return Future() { [weak self] promise in
            self?.coreDataDeleteService.delete(contentName) { isSucsess in
                if isSucsess {
                    promise(.success(true))
                } else {
                    promise(.failure(errorCoreData.contentEmpty))
                }
            }
        }
    }
}



extension CoreDataService {
    private func createPeristenceContainer() {
        container = NSPersistentContainer(name: "SocialMediaReminder")
        container.loadPersistentStores(completionHandler: { description, error in
            if error != nil {
                fatalError("erd not load")
            }
        })
    }
    
    private func insertingDependency(_ get:CoreDataGetProtocol,_ update: CoreDataUpdateProtocol, _ delete: CoreDataDeleteProtocol, _ insert: CoreDataInsertProtocol) {
        self.coreDataDeleteService = delete
        self.coreDataUpdateService = update
        self.coreDataInsertService = insert
        self.coreDataGetService = get
    }
}
